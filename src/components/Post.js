import React, { Component } from "react"
import { rhythm, scale } from "../utils/typography"
import Img from "gatsby-image"
import Link from "gatsby-link"
import styled from "styled-components"
import config from '../utils/config'

const Container = styled.div`
  font-size: .8rem;
  p {
    margin-bottom: ${rhythm(0.5)};
  }
  .read-more {
    display: flex;
    align-items: center;
    justify-content: flex-end;
    :before {
      transition: .3s all ${config.transition};
      content: "";
      display: inline-block;
      width: 0px;
      height: 1px;
      background: #000;
      margin-right: 5px;
    }
  }
  &:hover {
    .read-more {
      &:before {
        width: 30px;
      }
      /* box-shadow: inset 0 -0.5em 0 0 #f7f7f7; */
    }
    a {
      color: black;
    }
  }
`;

const Title = styled.h3`
  position: absolute;
`;

class Post extends Component {
  render() {
    const post = this.props.node
    return (
      <Container className={this.props.className}>
        <Link to={'/blog/' + post.slug + '/'}>
          <Img sizes={this.props.node.featured_media.localFile.childImageSharp.sizes} className="mb-3" />
          <h3 dangerouslySetInnerHTML={{ __html: post.title }} />
          <div className="row">
          <div className="col-6">{post.date}</div>
          <div className="col-6"><div className="read-more">Read more</div></div>
          </div>
        </Link>
      </Container>
    )
  }
}

export default Post