import React, { Component } from "react"
import { rhythm, scale } from "../utils/typography"
import Img from "gatsby-image"
import styled from 'styled-components'

const Wrapper = styled.div`
  opacity: 0.4;
  text-align: center;
  padding-top: ${props => props.nopad ? null : rhythm(1)};
  padding-bottom: ${props => props.nopad ? null : rhythm(1)};
  filter: grayscale(100%);
  .gatsby-image-wrapper {
    max-width: 200px;
    margin: 0 auto;
  }
`

class Client extends Component {
  render() {
    return (
      <Wrapper {...this.props}>
        <Img sizes={this.props.node.featured_media.localFile.childImageSharp.sizes} />
      </Wrapper>
    )
  }
}

export default Client