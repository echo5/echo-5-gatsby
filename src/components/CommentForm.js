import React, { Component } from 'react'
import styled from 'styled-components'
import { Formik } from 'formik'
import config from '../utils/config'
import { rhythm } from "../utils/typography"

const Alert = styled.div`
  padding: 12px 24px;
  background: ${props => props.type === 'error' ? '#f7236e' : '#6bbf8d'};
  color: white;
  margin-bottom: ${rhythm(1)};
`
const Error = styled.div`
  font-size: .7rem;
  color: #f7236e;
  position: absolute;
`

class CommentForm extends Component {  
  render () {
    return (
      <Formik
      initialValues={{
        author_name: '',
        author_email: '',
        content: '',
        author_url: '',
        post: this.props.post,
        parent: 0,
      }}
      validate={values => {
        let errors = {};
        if (!values.author_name) {
          errors.author_name = 'What should we call you?'
        }
        if (!values.author_email) {
          errors.author_email = 'Don\'t worry, we won\'t spam you'
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.author_email)
        ) {
          errors.author_email = 'We need a real email address :)';
        }
        if (!values.content) {
          errors.content = 'Please tell us your thoughts.'
        }
        return errors;
      }}
      onSubmit={(
        values,
        { setSubmitting, setErrors, setStatus, resetForm }
      ) => {
        const formData = new FormData();
        Object.keys(values).forEach(key => formData.append(key, values[key]));
        fetch(config.apiUrl + '/wp/v2/comments', {
          method: 'post',
          body: formData
        })
        .then((response) => {
          return response.json()
        })
        .then((data) => {
          console.log(data)
          setSubmitting(false)
          if (!data.message) {
            resetForm()
            setStatus({type: 'success', message: 'Thanks for your comment, we\'ll review and approve shortly!'})            
          } else {
            setStatus({type: 'error', message: data.message})
          }
        })
        .catch(error => {
          setSubmitting(false)
        });
      }}
      render={({
        values,
        errors,
        touched,
        status,
        setStatus,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
      <div>
        <h3>Leave a comment</h3>
        {status && status.message && <Alert type={status.type} dangerouslySetInnerHTML={{ __html: status.message }}></Alert>}
        <form onSubmit={handleSubmit} ref="commentForm">
          <div className="row narrow-gutters">
            <div className="col-md-6">
              <div className="form-group">
                <input 
                  type="text"
                  name="author_name"
                  placeholder="Name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.name} 
                />
                {errors.author_name && touched.author_name && <Error>{errors.author_name}</Error>}
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <input
                  type="email" 
                  name="author_email"
                  placeholder="Email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email} 
                />
                {errors.author_email && touched.author_email && <Error>{errors.author_email}</Error>}
              </div>
            </div>
          </div>
          <div className="form-group">
            <input
              type="website" 
              name="author_url"
              placeholder="Website"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.author_url} 
            />
            {errors.author_url && touched.author_url && <Error>{errors.author_url}</Error>}
          </div>
          <div className="form-group">
            <textarea
              name="content"
              placeholder="Content"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.content} 
            />
            {errors.content && touched.content && <Error>{errors.content}</Error>}
          </div>
          <input type="hidden" name="post" value={values.post} />
          <input type="hidden" name="parent" value={values.parent} />
          <input className="btn btn-primary" type="submit" value="Submit" disabled={isSubmitting} />
        </form>
      </div>          
      )}
      />
    )
  }
}

export default CommentForm