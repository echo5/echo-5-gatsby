import React, { Component } from "react"
import { rhythm, scale } from "../utils/typography"
import Link from "gatsby-link"
import styled from 'styled-components'
import config from "../utils/config"

const Overlay = styled.div`
  padding: ${rhythm(.3)} ${rhythm(.8)};
  align-items: center;
  justify-items: center;
  justify-content: center;
  background: #00000094;
  position: fixed;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  z-index: 101;
  /* display: ${state => state.isOpen ? 'flex' : 'none'}; */
  display: none;
  &.active {
    display: flex;
  }
`;
const Wrapper = styled.div`
  padding: 0;
  display: flex;
  justify-items: center;
  justify-content: center;
  background: black;
  height: 400px;
  max-height: 90vh;
  background: white;
  width: 600px;
  max-width: 100vh;
`;

class Modal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false
    }
  }
  openModal() {
    this.setState({
      isOpen: true
    })
  }
  closeModal() {
    this.setState({
      isOpen: false
    })
  }
  render() {

    // if (!this.state.isOpen) return null

    return (
      <Overlay onClick={this.closeModal.bind(this)} className={this.state.isOpen ? 'active' : 'hidden'}>
        <Wrapper>
          {this.props.children}
        </Wrapper>
      </Overlay>
    )
  }
}

export default Modal