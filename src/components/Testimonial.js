import React, { Component } from "react"
import { rhythm, scale } from "../utils/typography"
import Img from "gatsby-image"
import styled from "styled-components";
import config from '../utils/config'

const Wrapper = styled.div`
  text-align: center;
  float: left;
  blockquote {
    font-size: ${scale(-0.1).fontSize};    
    margin-left: 0rem;
    margin-right: 0rem;
    @media screen and (min-width: 700px) {
      font-size: ${scale(.4).fontSize};
      margin-left: 2.25rem;
      margin-right: 2.25rem;
    }
    line-height: 1.3;
    font-style: normal;
    position: relative;
    @media screen and (min-width: 700px) {    
      &:before {
        content: '“';
        position: absolute;
        left: -35px;
        top: -5px;
        font-size: 3rem;
        color: ${config.primaryColor};
        line-height: 1;
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif
      }
    }
  }
  img {
    border-radius: 12px;
  }
  @media screen and (max-width: 767px) {
    .gatsby-image-wrapper {
      max-width: 80px;
    }
  }
  h3 {
    margin-top: 15px;
  }
`

class Testimonial extends Component {
  render() {
    const testimonial = this.props.node
    return (
      <Wrapper>
        <blockquote dangerouslySetInnerHTML={{ __html: testimonial.content }}>
        </blockquote>
        <Img resolutions={this.props.node.featured_media.localFile.childImageSharp.resolutions} />
        <h3 dangerouslySetInnerHTML={{ __html: testimonial.title }} />
      </Wrapper>
    )
  }
}

export default Testimonial