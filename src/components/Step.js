import React, { Component } from "react"
import { rhythm, scale } from "../utils/typography"
import Img from "gatsby-image"
import styled from 'styled-components'

const Wrapper = styled.div`
  h2 {
    position: relative;
    line-height: 1;
    font-size: ${scale(1).fontSize};    
  }
  .step-number {
    right: 100%;
    margin-right: 20px;
    bottom: 5px;
    opacity: .3;
    font-size: ${scale(0.3).fontSize};
    @media(min-width:768px) {
      position: absolute;
    }
  }
`

class Step extends Component {
  render() {
    const step = this.props.node
    return (
      <Wrapper>
        <img src={step.image} />
        <h2 dangerouslySetInnerHTML={{ __html: '<span class="step-number">0' + step.id + '</span>' + step.name }} />
        <p dangerouslySetInnerHTML={{ __html: step.description }} />
      </Wrapper>
    )
  }
}

export default Step