import React, { Component } from 'react'
import styled from 'styled-components'
import Comments from '../components/Comments'
import CommentForm from '../components/CommentForm'

const Wrapper = styled.div`
  margin-bottom: 40px;
  width: 100%;
  .mr-3 {
    img {
      max-width: 48px;
      border-radius: 50%;
    }
  }
  .right {
    flex: 1;
  }
`

class Comment extends Component {
  render() {

    return (
      <Wrapper>
        <div className="d-flex">
          <div className="mr-3">
            <img src={this.props.node.author_avatar_urls.wordpress_96} />
          </div>
          <div className="right">
            <div className="comment-meta mb-3">
              <a rel="nofollow" href={this.props.node.author_url}>
                <span dangerouslySetInnerHTML={{ __html: this.props.node.author_name }} />
              </a>
              <span dangerouslySetInnerHTML={{ __html: this.props.node.date}} className="float-right" />
            </div>
            <div dangerouslySetInnerHTML={{ __html: this.props.node.content}} />
          </div>
        </div>   
        {this.props.node.children && this.props.node.wordpress_parent === 0 &&
          <Comments className="pl-3 mt-3" comments={this.props.node.children} />
        }
        {/* <CommentForm  /> */}
      </Wrapper>
    )
  }
}

export default Comment
