import React, { Component } from "react"
import { rhythm, scale } from "../utils/typography"
import Img from "gatsby-image"
import Link from "gatsby-link"
import styled from 'styled-components'
import config from '../utils/config'

const Wrapper = styled.div`
  position: relative;
  margin-bottom: ${rhythm(2)};

  .blob {
    z-index: -1;
    position: absolute;
    top: 0;
    left: 20px;
    content: '';
    display: block;
    background: purple;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    transform: skewY(-8deg);
    opacity: .3;
    transition: transform .4s ease-in-out;
    animation: skewing 4s 0s ease-in-out infinite;
    animation-play-state: paused; 
    animation-fill-mode: forwards;

  }

  .blob-2 {
    left: 30px;
    top: 2px;
    transform: skewY(35deg) scale(.8);
  }

  &:hover .blob-1 {
    animation-play-state: running; 
  }

  @keyframes skewing {
    0% { transform: skewX(8deg); }
    10% { transform: skewX(-8deg); }
    20% { transform: skewX(4deg); }
    30% { transform: skewX(-4deg); }
    40% { transform: skewX(10deg); }
    50% { transform: skewX(-6deg); }
    55% { transform: skewX(8deg); }
    60% { transform: skewX(-5deg); }
    65% { transform: skewX(5deg); }
    70% { transform: skewX(-8deg); }
    75% { transform: skewX(8deg); }
    80% { transform: skewX(-3deg); } 
    85% { transform: skewX(3deg); }
    90% { transform: skewX(-6deg); } 
    95% { transform: skewX(6deg); }
    100% { transform: skewX(1deg); }
  }

  a {
    transition: .3s ${config.transition};
  }
  li:hover a {
    padding-left: 20px;
  }

  svg {
    width: 55px;
    height: auto;
  }
  h3 {
    margin-top: ${rhythm(.3)};
  }
`

class Service extends Component {
  render() {
    const service = this.props.node
    return (
      <Wrapper>
          <div dangerouslySetInnerHTML={{ __html: service.acf.icon }} />
          <div className="blob blob-1" style={{ backgroundColor: service.acf.color }} />
          <div className="blob blob-2" style={{ backgroundColor: service.acf.color }} />
          <Link to={'/services/' + service.slug + '/'}>
            <h3 dangerouslySetInnerHTML={{ __html: service.title }} />
          </Link>
          {this.props.children.length === 0 && 
            <p dangerouslySetInnerHTML={{ __html: service.excerpt }} />
          }
          <ul>
          {this.props.children.map(({ node }) => (
            <li key={node.slug}>
              {node.excerpt?
              <Link to={'/services/' + node.slug + '/'}>
                <span dangerouslySetInnerHTML={{ __html: node.title }} />
              </Link>
              :
              <span dangerouslySetInnerHTML={{ __html: node.title }} />}
            </li>
          ))}
          </ul>
      </Wrapper>
    )
  }
}

export default Service