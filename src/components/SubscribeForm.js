import React, { Component } from "react"
import styled from 'styled-components'
import { Formik } from 'formik'
import config from '../utils/config'

const Alert = styled.div`
  padding: 12px 24px;
  background: ${props => props.type === 'error' ? '#f7236e' : '#6bbf8d'};
  color: white;
`

const Error = styled.div`
  font-size: .7rem;
  color: #f7236e;
  position: absolute;
  bottom: 100%;
`

class SubscribeForm extends Component {

  render() {
    return (
      <Formik
      initialValues={{
        fullname: '',
        email: '',
      }}
      validate={values => {
        let errors = {};
        if (!values.fullname) {
          errors.fullname = 'What should we call you?'
        }
        if (!values.email) {
          errors.email = 'Don\'t worry, we won\'t spam you'
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
        ) {
          errors.email = 'We need a real email address :)';
        }
        return errors;
      }}
      onSubmit={(
        values,
        { setSubmitting, setErrors, setStatus, resetForm }
      ) => {
        const formData = new FormData();
        Object.keys(values).forEach(key => formData.append(key, values[key]));
        fetch(config.apiUrl + '/contact-form-7/v1/contact-forms/825/feedback', {
          method: 'post',
          body: formData
        })
        .then((response) => {
          return response.json()
        })
        .then((data) => {
          setSubmitting(false)
          if (data.status === 'mail_sent') {
            dataLayer.push({'event' : 'form_submitted'})            
            resetForm()
          }
          setStatus({type: data.status === 'mail_sent' ? 'success' : 'error', message: data.message})
        })
        .catch(error => {
          setSubmitting(false)
        });
      }}
      render={({
        values,
        errors,
        touched,
        status,
        setStatus,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
      <div className="container">
        <div className="div">
          Subscribe for free business insights and guides
        </div>
        {status && status.message && <Alert className="ml-auto" type={status.type}>{status.message}</Alert>}
        {!status &&
        <form onSubmit={handleSubmit} ref="subscribeForm" className="ml-auto">
          <div className="form-group">
            <input 
              type="text"
              name="fullname"
              placeholder="Name"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.fullname} 
            />
            {touched.fullname && errors.fullname && <Error>{errors.fullname}</Error>}
          </div>
          <div className="form-group">
            <input
              type="email" 
              name="email"
              placeholder="Email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email} 
            />
            {errors.email && touched.email && <Error>{errors.email}</Error>}
          </div>
          <input className="btn btn-primary ml-3" type="submit" value="Subscribe" disabled={isSubmitting} />
        </form>
        }            
      </div>          
      )}
      />
    )
  }
}

export default SubscribeForm