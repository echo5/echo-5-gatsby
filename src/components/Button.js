import React, { Component } from "react"
import { rhythm, scale } from "../utils/typography"
import Link from "gatsby-link"
import styled from 'styled-components'
import config from "../utils/config"

const Button = styled(Link)`
  position: relative;
  overflow: hidden;
  color: palevioletred;
  display: block;
  padding: ${rhythm(.35)} ${rhythm(.8)};
  display: inline-flex;
  border-radius: 0px;
  justify-items: center;
  justify-content: center;
  color: white;
  background: ${config.primaryColor};
  align-items: center;
  img, svg {
    height: 20px;
    width: 20px;
    margin: 0 10px 0 0;
  }
  &:hover {
    color: white;
    &:before {
      opacity: 0;
      transform: translate(0,0);
    }
  }
  &:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 1;
    transition: all 0.5s;
    opacity: 1;
    transform: translate(-105%,0);
    border-right-width: 1px;
    border-right-style: solid;
    border-right-color: rgba(255,255,255,1);
    background-color: rgba(255,255,255,0.25);
  }
`;

class ButtonComponent extends Component {
  render() {
    return (
      <Button
        {...this.props}
        to={this.props.to}>
        {this.props.children}
      </Button>
    )
  }
}

export default ButtonComponent