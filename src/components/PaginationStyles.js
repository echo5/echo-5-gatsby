import React, { Component } from "react"
import styled from 'styled-components'
import { css } from 'styled-components'

const paginationStyles = css`
  display: flex;
  padding: 0;
  justify-content: center;
  margin-top: 40px;
  list-style: none;
  li {
    padding-left: 0 !important;
  }
  li:before {
    display: none !important;
  }
  a {
    padding: 10px 15px;      
    position: relative;
    cursor: pointer;
    &.active {
        cursor: default;
    }
    &.active:after {
        transform-origin: center right;
        transform: scaleX(1);            
    }
    &:after {
        content: '';
        position: absolute;
        display: block;
        width: 80%;
        height: 2px;
        top: 50%;
        left: 10%;
        margin-top: 0px;
        background: black;
        transform-origin: center left;
        transform: scaleX(0);
        transition: transform 0.5s cubic-bezier(.55, 0, .1, 1);            
    }
  }
`

export default paginationStyles