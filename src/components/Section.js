import React, { Component } from "react"
import { rhythm, scale } from "../utils/typography"
import styled from 'styled-components'
import { css } from 'styled-components'

// .bg-overlay {
//   content: null;
//   display: block;
//   width: 100%;
//   height: 100%;
//   background: #33343a26;
//   position: absolute;
//   top: 0;
// }


const Wrapper = styled.div`
  position: relative;
  overflow: hidden;
  padding-top: ${props => props.nopad ? null : rhythm(2)};
  padding-bottom: ${props => props.nopad ? null : rhythm(2)};
  @media(min-width:768px) {
    padding-top: ${props => props.nopad ? null : rhythm(4.5)};
    padding-bottom: ${props => props.nopad ? null : rhythm(4.5)};
    padding-top: ${props => props.hero ? rhythm(5.5) : null};
    padding-bottom: ${props => props.hero ? rhythm(5.5) : null};
  }

  background-image: ${props => props.bg ? `url('${props.bg}')` : null}; 
  background-position: center;
  background-color: ${props => props.gray ? '#f7f7f9' : null};
  background-color: ${props => props.dark ? 'black' : null};
  background-color: ${props => props.bgColor ? props.bgColor : null};
  background-size: cover;
  min-height: ${props => props.tall ? '80vh' : null};
  min-height: ${props => props.half ? '50vh' : null};
  display: flex;
  align-items: flex-end;

  @media(max-width: 768px) {
    ${props => !props.nopad && css`    
      &:first-child {
        padding-top: 6rem;
      }
    `}
  }

  ${props => (props.dark || props.overlay) && css`
    color: white;
    h1, h2, h3, h4, h5, h6 {
      color: white;
    }
    .line {
      &:after {
        background: currentColor;
      }
    }
  `}

  > div {
    z-index: 1;
  }
  ${props => props.overlay && css`
    &:before {
      content: '';
      display: block;
      width: 100%;
      height: 100%;
      background: #33343a66;
      position: absolute;
      top: 0;
    }
  `}
`

class Container extends Component {
  render() {
    const data = this.props.data
    const classes = this.props.dark ? 'dark' : 'light'

    return (
      <Wrapper {...this.props}>
        <div className={this.props.full ? 'container-fluid' : 'container'}>
          {this.props.children}
        </div>
      </Wrapper>
    )
  }
}

export default Container