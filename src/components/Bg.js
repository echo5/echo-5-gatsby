import React, { Component } from "react"
import { rhythm, scale } from "../utils/typography"
import styled from "styled-components"
import { css } from 'styled-components'

class Background extends Component {
  render() {
    const data = this.props.data
    const classes = this.props.dark ? 'dark' : 'light'

    const Bg = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    left: ${this.props.right ? 'auto' : null};
    right: ${this.props.right ? this.props.right : null};
    width: ${this.props.width ? this.props.width : '30%'};
    height: ${this.props.h ? this.props.h : '100%'};
    background-color: ${this.props.bgColor ? this.props.bgColor : '#f7f7f7'};
    background-image: url('${this.props.img ? this.props.img : null}');
    z-index: -1;
    overflow: ${this.props.overflow ? this.props.overflow : 'hidden'};
    background-position: center;
    background-size: cover;

    ${props => props.overlay && css`
      &:before {
        content: '';
        display: block;
        width: 100%;
        height: 100%;
        background: #33343a56;
        position: absolute;
        top: 0;
        z-index: 10;
      }
    `}

    ${props => props.cut && css`
      &:after {
        content: '';
        display: block;
        width: 50%;
        height: 20%;
        background: #fff;
        position: absolute;
        bottom: 0;
        z-index: 10;
      }
    `}

    ${props => props.fill && css`
      .gatsby-image-outer-wrapper {
        position: absolute !important;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        .gatsby-image-wrapper {
          height: 100% !important;
          max-width: 100%;
        }
        img {
          height: 100% !important;
        }
      }
    `}

    
  `;

    return (
      <Bg {...this.props}>
          {this.props.children}
      </Bg>
    )
  }
}

export default Background