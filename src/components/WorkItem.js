import React, { Component } from "react"
import { rhythm, scale } from "../utils/typography"
import Img from "gatsby-image"
import Link from "gatsby-link"
import styled from "styled-components"

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: relative;
  overflow: hidden;
  h3 {
    position: absolute;
    background: white;
    margin: 30px;
    bottom: 0;
    left: 0;
    padding: 10px 20px;
  }
  &:hover {
    .gatsby-image-wrapper {
      transform: scale(.8);
      img {
        transform: scale(1.3);
      }
    }
  }
  .gatsby-image-wrapper {
    display: block !important;
    transition: all .4s cubic-bezier(0.3, 0.1, 0.45, 1);
    img {
      transition: all .4s cubic-bezier(0.3, 0.1, 0.45, 1) !important;
    }
  }
`;

const Title = styled.h3`
  position: absolute;
`;

class WorkItem extends Component {
  render() {
    const item = this.props.node
    return (
      <Container className={this.props.className}>
        <Link to={'/work/' + item.slug + '/'}>
          <Img resolutions={this.props.node.featured_media.localFile.childImageSharp.resolutions} />
          <h3 dangerouslySetInnerHTML={{ __html: item.title }} />
        </Link>
      </Container>
    )
  }
}

export default WorkItem