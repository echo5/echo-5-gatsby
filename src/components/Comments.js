import React, { Component } from "react"
import styled from 'styled-components'
import Comment from '../components/Comment'

const Alert = styled.div`
  padding: 12px 24px;
  background: ${props => props.type === 'error' ? '#f7236e' : '#6bbf8d'};
  color: white;
`

class Comments extends Component {

  render() {

    return (
      <div className={this.props.className}>
        {this.props.comments.map(({ node }) => (
          <Comment node={node} key={node.id} />
        ))}
      </div>
    )
  }
}

export default Comments
