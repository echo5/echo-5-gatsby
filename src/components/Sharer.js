import React, { Component } from "react"
import { rhythm, scale } from "../utils/typography"
import styled, {css} from "styled-components"
import Linkedin from '../images/linkedin.svg'
import Facebook from '../images/facebook.svg'
import Twitter from '../images/twitter.svg'
import Googleplus from '../images/googleplus.svg'
import config from '../utils/config'

class Sharer extends Component {
  handleScroll() {
    if (this.props.parent && false) {
      var top = this.props.parent.getBoundingClientRect().top + document.documentElement.scrollTop
      var bottom = top + this.props.parent.clientHeight

      if (window.pageYOffset > top && window.pageYOffset < bottom) {
        console.log('stuck')
        this.setState({ 
          isStuck: true,
          top: window.pageYOffset        
        });
      } else {
        console.log('unstuck')
        this.setState({
          isStuck: false
        })
      }
    }    
  }
  componentDidMount () {
    window.addEventListener('scroll', this.handleScroll.bind(this), false);
  }
  componentWillUnmount () {
    window.removeEventListener('scroll', this.handleScroll.bind(this));
  }
  constructor(props) {
    super(props)
    this.state = {
      isStuck: false,
      top: 100
    }
  }
  render() {

    const Wrapper = styled.div`
      display: inline-block;
      width: 100%;
      @media (min-width: 1000px) {
        position: ${props => props.isStuck ? 'fixed' : 'absolute'};
        top: 0;
        margin-left: -180px;
        width: 80px;
      }
      a {
        width: 60px;
        height: 60px;
        line-height: 65px;
        display: block;
        margin-bottom: 12px;
        margin-right: 18px;
        position: relative;
        text-align: center;
        float: left;
        @media (min-width: 1000px) {
          clear: both;
        }
        &:hover {
          &:after {
            top: 8px;
            left: 8px;
          }
        }
        svg {
          position: relative;
          z-index: 10;
          max-width: 24px;
          fill: black !important;
        }
        &:after, &:before {
          content: '';
          position: absolute;
          top: 0px;
          left: 0px;
          width: 60px;
          height: 60px;
          z-index: 2;
          background: white;      
          border: 2px solid black;              
        }
        &:after {
          z-index: 1;
          top: 6px;
          left: 6px;
          background: blue;
          transition: .2s ${config.transition};     
        }
        &.twitter {
          &:after {
            background: #55acee;
          }
          svg {
            fill: #55acee;
          }
        }
        &.linkedin {
          &:after {
            background: #0077B5;
          }
          svg {
            fill: #0077B5;
          }
        }
        &.facebook {
          &:after {
            background: #3b5999;
          }
          svg {
            fill: #3b5999;
          }
        }
        &.googleplus {
          &:after {
            background: #dd4b39;
          }
          svg {
            fill: #dd4b39;
          }
        }
        
      }
    `;

    return (
      <Wrapper isStuck={this.state.isStuck}>
        <a className="linkedin" href={`https://www.linkedin.com/shareArticle?mini=true&url=${encodeURI(this.props.url)}&title=${this.props.title}&summary=${this.props.text}`}><Linkedin /></a>
        <a className="facebook" href={`https://www.facebook.com/sharer/sharer.php?u=${encodeURI(this.props.url)}`}><Facebook /></a>
        <a className="googleplus" href={`https://plus.google.com/share?url=${encodeURI(this.props.url)}&text=${this.props.text}`}><Googleplus /></a>
        <a className="twitter" href={`https://twitter.com/intent/tweet?url=${encodeURI(this.props.url)}&text=${this.props.title}`}><Twitter /></a>
      </Wrapper>
    )
  }
}

export default Sharer