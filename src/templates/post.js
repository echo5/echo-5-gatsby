import React, { Component } from "react"
import Helmet from 'react-helmet'
import config from '../utils/config'
import PropTypes from "prop-types"
import Section from "../components/Section"
import Bg from "../components/Bg"
import PostIcons from "../components/PostIcons"
import Img from "gatsby-image"
import styled from 'styled-components'
import { rhythm } from "../utils/typography"
import { Formik } from 'formik'
import SubscribeForm from '../components/SubscribeForm'
import Comments from '../components/Comments'
import CommentForm from '../components/CommentForm'
import Sharer from '../components/Sharer'
import Sticky from 'react-stickynode';

const PostContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const PostContent = styled.div`
  h2, h3, h4 {
    margin-top: ${rhythm(2)};
  }
  blockquote  {
    border-left: 3px solid black;
    padding: 10px 20px;
    background: #f7f7f7;
  }
`

const SocialShare = styled.div`
@media (max-width:999px){
  order: 2;
  .sticky-inner-wrapper {
    position: static!important;
    transform: none !important;
    width: auto !important;
  }
}
`

const CtaFixed = styled.div`
  display: none;
  @media(min-width: 992px) {
    display: block;
    transform: translate3d(0,${props => props.isPostScrolled ? '0%' : '100%'},0);
    transition: .2s ${config.transition};
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    z-index: 10;
    background: white;
    border-top: 3px solid black;
    padding: 15px 0;
    form {
      display: flex;
      align-items: center;
      margin: 0;
    }
    .container {
      display: flex;
      align-items: center;
    }
    .form-group {
      max-width: 240px;
      margin: 0 0 0 20px;
    }
    input[type=submit] {
      max-width: 180px;
      background: ${config.primaryColor};
      color: white;
      border: 0;
    }
  }  
`

class PostTemplate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isPostScrolled: false,
      comments: [],
      clientHeight: 0,
    }
    this.scrollFn = this.handleScroll.bind(this)
  }
  componentDidMount() {
    this.setState({
      contentHeight: this.postContent.clientHeight
    })
    window.addEventListener('scroll', this.scrollFn)
    if (this.state.comments.length == 0 && this.props.data.allWordpressWpComments) {
      const comments = this.nestComments(this.props.data.allWordpressWpComments.edges)
      this.setState({
        comments: comments
      })
    }
  }
  handleScroll() {
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop
    if (scrollTop / this.state.contentHeight > 0.6) {
      this.setState({
        isPostScrolled: true
      })
    } else {
      this.setState({
        isPostScrolled: false
      })
    }
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollFn)
  }
  nestComments(commentList) {
    const commentMap = {};

    // move all the comments into a map of id => comment
    commentList.forEach(comment => commentMap[comment.node.wordpress_id] = comment);

    // iterate over the comments again and correctly nest the children
    commentList.forEach(comment => {
      if(comment.node.wordpress_parent !== 0) {
        const parent = commentMap[comment.node.wordpress_parent];
        parent.node.children = parent.node.children || []
        parent.node.children.push(comment);
      }
    });

    // filter the list to return a list of correctly nested comments
    return commentList.filter(comment => {
      return comment.node.wordpress_parent === 0;
    });
  }
  render() {
    const post = this.props.data.wordpressPost
    
    const schemaOrg = {
      "@context": "http://schema.org",
      "@type": "BlogPosting",
      "headline": post.title,
      "image": post.featured_media.source_url,
      "publisher": {
        "@type": "Organization",
        "name": config.siteTitle,
        "url": config.siteUrl,
        "logo": {
          "@context": "http://schema.org",
          "@type": "ImageObject",
          "url": config.staticLogo,
        }
      },
      "dateModified":post.modified,
      "datePublished": post.date,
      "author": post.author.name,
      "description": post.yoast.metadesc || post.excerpt,
      "mainEntityOfPage": config.siteUrl + '/blog/' + post.slug + '/'
    }

    return (
      <div>
        <Helmet
          title={post.title}
          meta={[
          { name: 'description', content: post.yoast.metadesc || post.excerpt },
          ]}
        >
          <script type="application/ld+json">
            {JSON.stringify(schemaOrg)}
          </script>
        </Helmet>          
        <Section>
          <div className="row">
            <div className="col-md-10 mx-auto mb-md-5">
              <PostIcons node={post} className="mb-3" />
              <h1 dangerouslySetInnerHTML={{ __html: post.title }} />
            </div>
          </div>
          <div className="row">
            <div className="col-lg-9 col-xl-8 mx-auto" >
              <PostContentWrapper id="post-content" className="post-content" innerRef={element => { this.postContent = element }}>
                {/* <Img sizes={post.featured_media.localFile.childImageSharp.sizes} /> */}
                <SocialShare>
                  <Sticky top={150}>
                    <Sharer url={config.siteUrl + '/blog/' + post.slug + '/'} title={post.title} text={post.yoast.metadesc || post.excerpt} />                
                  </Sticky>
                </SocialShare>
                <PostContent dangerouslySetInnerHTML={{ __html: post.content }} />
              </PostContentWrapper>
          
              
              <div className="mt-5">
                <div className="mb-5">
                  <CommentForm post={post.wordpress_id} />
                </div>
                <Comments comments={this.state.comments} />              
              </div>
            </div>
          </div>
        </Section>

        <CtaFixed isPostScrolled={this.state.isPostScrolled}>
          <SubscribeForm />
        </CtaFixed>

      </div>
    )
  }
}
//<img src={post.image.sizes.thumbnail} />

PostTemplate.propTypes = {
  data: PropTypes.object.isRequired,
  edges: PropTypes.array,
}

export default PostTemplate

export const pageQuery = graphql`
  query currentPostQuery($id: String!, $postId: Int!) {
    wordpressPost(id: { eq: $id }) {
      wordpress_id
      slug
      title
      content
      excerpt
      author {
        name
      }
      modified
      featured_media {
        source_url
        localFile {
          childImageSharp {
            sizes(maxWidth: 800, maxHeight: 500) {
              ...GatsbyImageSharpSizes
            }
          }
        }
      }
      yoast {
        metadesc
      }
      ...PostIcons
    }
    allWordpressWpComments(filter: { post: {eq: $postId }}) {
      edges {
        node {
          id
          author_name
          content
          post
          wordpress_parent
          wordpress_id
          date(formatString: "MMMM DD, YYYY")
          author_url
          author_avatar_urls {
            wordpress_48
            wordpress_96
          }
        }
      }
    }
    site {
      siteMetadata {
        title
        subtitle
      }
    }
  }
`
