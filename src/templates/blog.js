import React, { Component } from "react"
import PropTypes from "prop-types"
import Section from "../components/Section"
import Post from "../components/Post"
import Bg from "../components/Bg"
import Link from "gatsby-link"
import styled from 'styled-components'
import { rhythm } from "../utils/typography"
import Helmet from 'react-helmet'
import config from '../utils/config'
import PaginationStyles from '../components/PaginationStyles'

const PostContainer = styled.div`
  margin-bottom: ${rhythm(3)};
  @media (min-width: 768px) {
    &:nth-child(even) {
      margin-top: 80px;
    }
  }
`
const Pagination = styled.ul`${PaginationStyles}`

const NavLink = props => {
  if (props.totalPages && (props.page > props.totalPages || props.page < 1)) {
    return null
  }
  return <li><Link exact activeClassName="active" to={'/blog/' + (props.page === 1 ? '' : props.page) + '/'}>{props.text}</Link></li>;
};


export default ({data, pathContext}) => {
  const { group, index, first, last, pageCount } = pathContext;
  const previousUrl = index - 1 == 1 ? "/blog" : (index - 1).toString();
  const nextUrl = '/blog/' + (index + 1).toString();
  var posts = []
  {group.map(({ node }) => (
    posts.push(
      {
        "@type": "blogPosting",
        "mainEntityOfPage": "https://echo5digital.com/blog/" + node.slug + "/",
        "headline": node.title,
        "author": node.author.name,
        "datePublished": node.date,
        "image": node.featured_media.localFile.childImageSharp.src
      }
    )
  ))}
  const schemaOrg = {
    "@context": "http://schema.org",
    "@type": "Blog",
    "name": "Echo 5 Digital Blog",
    "url": "https://echo5digital.com/blog",
    "description": "Our thoughts about web design, web development, how to increase website ranking and sales, and how to build and improve websites.",
    "publisher": {
      "@type": "Organization",
      "name": "Echo 5 Digital"
    },
    "blogPosts": posts
  }

  return (
    <div>
      <Helmet
        title={`Blog`}
        meta={[
        { name: 'description', content: 'Our thoughts about web design, web development, how to increase website ranking and sales, and how to build and improve websites.' },
        ]}
      >
      </Helmet>       
      <Section className="pb-0">
        <div className="row">
          <div className="col-md-9">
            <h1>Our ramblings on web design &amp; development.</h1>
          </div>
        </div>
      </Section>  
      <Section>
        <Bg />
        <div className="row">
          <div className="col-lg-12 col-xl-10">
            <div className="row wide-gutters">          
              {group.map(({ node }) => (
                <PostContainer className="col-md-6" key={node.id}>
                  <Post node={node} />
                </PostContainer>
              ))}
            </div>
          </div>
        </div>
        <Pagination>
          <NavLink currentPage={index} page={index - 1} totalPages={pageCount} text="Newer" />
          {Array.apply(null, {length: pageCount}).map(( page, pageI ) => (
            <NavLink currentPage={index} page={pageI + 1} text={pageI + 1} key={pageI} />            
          ))}
          <NavLink currentPage={index} page={index + 1} totalPages={pageCount} text="Older" />
        </Pagination>
      </Section>
    </div>
  )
}


export const pageQuery = graphql`
  query blogPageQuery($index: Int) {
    allWordpressPost(skip: $index, limit: 10, sort: { order: DESC, fields: [date] }) {
      edges {
        node {
          id
          title
          excerpt
          slug
          date(formatString: "MMMM DD, YYYY")
          author {
            name
          }
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 500, maxHeight: 280) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
        }
      }
    }
  }
`
