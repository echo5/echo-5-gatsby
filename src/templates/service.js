import React, { Component } from "react"
import PropTypes from "prop-types"
import Img from "gatsby-image"
import Link from "gatsby-link"
import Helmet from 'react-helmet'
import config from '../utils/config'
import Section from '../components/Section'
import Bg from '../components/Bg'
import webdesign from '../images/webdesign.jpeg'
import coffee from '../images/coffee.jpeg'

import { rhythm } from "../utils/typography"

class ServiceTemplate extends Component {
  render() {
    const service = this.props.data.wordpressWpServices
    const schemaOrg = {
      "@context": "http://schema.org",
      "@type": "Service",
      "name": service.title,
      "provider": {
        "@type": "Organization",
        "name": config.siteTitle,
        "url": config.siteUrl,
        "logo": {
          "@context": "http://schema.org",
          "@type": "ImageObject",
          "url": config.staticLogo,
        }
      },
      "description": service.yoast.metadesc || service.excerpt,
      "mainEntityOfPage": config.siteUrl + '/services/' + service.slug + '/'
    }

    return (
      <div>
        <Helmet 
          title={`${service.title}`} 
          meta={[
          { name: 'description', content: service.yoast.metadesc || service.excerpt },
          ]}
        >
          <script type="application/ld+json">
            {JSON.stringify(schemaOrg)}
          </script>
        </Helmet>   
        <Section>
          <div className="row">
            <div className="col-md-8">
              <h1 dangerouslySetInnerHTML={{ __html: service.title }} />
              <div dangerouslySetInnerHTML={{ __html: service.content }} />          
            </div>
          </div>
        </Section>
        <Section half>
          <Bg fill width="80%" bgColor={service.acf.color}>
            <Img resolutions={service.featured_media.localFile.childImageSharp.resolutions} />
          </Bg>
          {/* <img src={webdesign} /> */}
        </Section>
        <Section>
          <div className="row">
            <div className="col-md-7 offset-md-4">
              <h2>Benefits of {service.title}</h2>
              <div dangerouslySetInnerHTML={{ __html: service.acf.why }} />          
            </div>
          </div>
        </Section>
        <Section>
          <Bg right="0" />
          <div className="row">
            <div className="col-md-6">
              <h2>Our Process</h2>
              <div dangerouslySetInnerHTML={{ __html: service.acf.our_process }} />                      
            </div>
          </div>
        </Section>
        
        <Section dark bgColor="transparent" style={{marginTop: '5rem'}}>
          <Bg bgColor='#000' width="80%" />          
          <div className="row">
            <div className="col-8">
              <h2>Ready to ramp up your {service.title.toLowerCase()}?</h2>
              <h3><Link className="line" style={{ color: config.primaryColor, textDecoration: 'underline'}} to="/contact">Let's talk</Link></h3>
            </div>
          </div>
        </Section>

      </div>
    )
  }
}

ServiceTemplate.propTypes = {
  data: PropTypes.object.isRequired,
  edges: PropTypes.array,
}

export default ServiceTemplate

export const pageQuery = graphql`
  query currentServiceQuery($id: String!) {
    wordpressWpServices(id: { eq: $id }) {
      title
      content
      excerpt
      yoast {
        metadesc
      }
      acf {
        color
        why
        our_process
      }
      featured_media {
        localFile {
          childImageSharp {
            resolutions(width: 1900) {
              ...GatsbyImageSharpResolutions_withWebp_tracedSVG
            }
          }
        }
      }
    }
    site {
      siteMetadata {
        title
        subtitle
      }
    }
  }
`
