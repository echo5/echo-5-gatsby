import React, { Component } from "react"
import PropTypes from "prop-types"
import Link from "gatsby-link"
import Img from "gatsby-image"
import Bg from '../components/Bg'
import Section from '../components/Section'
import { rhythm } from "../utils/typography"
import Helmet from 'react-helmet'
import config from '../utils/config'

class WorkItemTemplate extends Component {
  render() {
    const item = this.props.data.wordpressWpWork

    const schemaOrg = {
      "@context": "http://schema.org",
      "@type": "WebSite",
      "headline": item.title,
      "image": item.featured_media.source_url,
      "publisher": {
        "@type": "Organization",
        "name": config.siteTitle,
        "url": config.siteUrl,
        "logo": {
          "@context": "http://schema.org",
          "@type": "ImageObject",
          "url": config.staticLogo,
        }
      },
      "dateModified":item.modified,
      "datePublished": item.date,
      "description": item.yoast.metadesc || item.excerpt,
      "mainEntityOfPage": config.siteUrl + '/work/' + item.slug + '/'
    }

    return (
      <div>
        <Helmet 
          title={`${item.title}`} bodyAttributes={{class: 'dark'}} 
          meta={[
          { name: 'description', content: item.yoast.metadesc || item.excerpt },
          ]}
          >
          <script type="application/ld+json">
            {JSON.stringify(schemaOrg)}
          </script>
        </Helmet>          
        <Section dark overlay tall>
          <Bg overlay fill width="100%">
            <Img className="fill" sizes={item.featured_media.localFile.childImageSharp.sizes} />          
          </Bg>
          <div className="row">
            <div className="col-md-9">
              <h1 dangerouslySetInnerHTML={{ __html: item.title }} />
            </div>
          </div>
        </Section>
        <div dangerouslySetInnerHTML={{ __html: item.content }} />

        <Section dark bgColor="transparent">
          <Bg bgColor='#000' width="80%" />          
          <div className="row">
            <div className="col-8">
              <h2>Ready to lead your industry?</h2>
              <h3><Link className="line" style={{ color: config.primaryColor, textDecoration: 'underline'}} to="/contact">Let's chat</Link></h3>
            </div>
          </div>
        </Section>

      </div>
    )
  }
}

WorkItemTemplate.propTypes = {
  data: PropTypes.object.isRequired,
  edges: PropTypes.array,
}

export default WorkItemTemplate

export const pageQuery = graphql`
  query currentWorkItemQuery($id: String!) {
    wordpressWpWork(id: { eq: $id }) {
      slug
      excerpt
      title
      content
      featured_media {
        localFile {
          childImageSharp {
            sizes(maxWidth: 1900, maxHeight: 1000) {
              base64
              aspectRatio
              src
              srcSet
              sizes
            }
          }
        }
      }
      yoast {
        metadesc
      }
    }
    site {
      siteMetadata {
        title
        subtitle
      }
    }
  }
`
