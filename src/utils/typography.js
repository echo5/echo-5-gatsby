import Typography from "typography"
import wordpress2013 from "typography-theme-wordpress-2013"

wordpress2013.headerLineHeight = 1.1
wordpress2013.includeNormalize = true
wordpress2013.headerWeight = 900
wordpress2013.headerColor = 'black'
wordpress2013.bodyColor = '#827f8e'
wordpress2013.baseFontSize = '24px'
wordpress2013.headerFontFamily = ['Sofia Pro', 'Helvetica Neue', 'Segoe UI', 'Helvetica', 'Arial', 'sans-serif']
wordpress2013.bodyFontFamily = ['aktiv-grotesk', 'Helvetica Neue', 'Segoe UI', 'Helvetica', 'Arial', 'sans-serif']
wordpress2013.googleFonts = false


wordpress2013.overrideThemeStyles = () => {
  return {
    body: {
      wordWrap: 'normal'
    },
    a: {
      color: `inherit`,
    },
    blockquote: {
      color: 'black',
      fontStyle: 'normal',
      marginRight: 0,
      marginLeft: 0,
    },
    modularScales: [
      {
        scale: 0.25,
      },
      {
        scale: 'octave',
        maxWidth: '768px',
      },
    ],
    // h1: {
    //   lineHeight: 1,
    // },
    // 'h2,h3': {
    //   marginBottom: rhythm(1/2),
    //   marginTop: rhythm(2),
    // }
  }
}

const typography = new Typography(wordpress2013)

export default typography
