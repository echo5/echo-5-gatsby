module.exports = {
  apiUrl: "https://content.echo5digital.com/wp-json",
  siteUrl: "https://echo5digital.com",
  siteTitle: "Echo 5 Digital",
  staticLogo: "https://content.echo5digital.com/assets/uploads/2018/04/logo.png",
  primaryColor: "#f4226b",
  secondaryColor: "#2ac0e2",
  transition: 'cubic-bezier(0.3, 0.1, 0.45, 1)',
  googleTagManagerID: 'GTM-KZDC5K8'
};