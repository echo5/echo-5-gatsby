import React, { Component } from "react"
import PropTypes from "prop-types"
import Section from "../components/Section"
import Bg from "../components/Bg"
import Service from "../components/Service"
import Link from "gatsby-link"
import Helmet from 'react-helmet'
import config from '../utils/config'

import { rhythm } from "../utils/typography"

class ServicePage extends Component {
  render() {
    const data = this.props.data
    const categories = data.allWordpressWpServices.edges.filter(service => service.node.wordpress_parent === 0)
    return (
      <div>
        <Helmet 
          title={`Digital Product Services`}
          meta={[
          { name: 'description', content: 'Our award-winning team provides the best web design service in Atlanta, Georgia.  We deliver beautiful and functional websites at a reasonable price.' },
          ]}
        />                      
        <Section className="pb-0">
          <div className="row">
            <div className="col-md-8">
              <h1>Things we do to grow your business</h1>
            </div>
          </div>
        </Section>
        
        <Section>
          <div className="row">
            <div className="col-md-4">
              <h4 className="line">How we help</h4>
            </div>
            <div className="col-md-8">
              <div className="row">
                {categories.map(({ node }) => (
                  <div className="col-md-6" key={node.id}>
                    <Service node={node} children={data.allWordpressWpServices.edges.filter(service => service.node.wordpress_parent === node.wordpress_id)} />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </Section>

        <Section dark bgColor="transparent">
          <Bg bgColor='#000' width="80%" />          
          <div className="row">
            <div className="col-8">
              <h2>Not sure which is right for you?</h2>
              <h3><Link className="line" style={{ color: config.primaryColor, textDecoration: 'underline'}} to="/contact">Ask us</Link></h3>
            </div>
          </div>
        </Section>
        
        
      </div>
    )
  }
}

export default ServicePage

export const pageQuery = graphql`
  query servicesPageQuery {
    allWordpressWpServices {
      edges {
        node {
          id
          slug
          title
          excerpt
          wordpress_parent
          wordpress_id
          acf {
            icon
            color
          }
        }
      }
    }
  }
`
