import React, { Component } from "react"
import Link from "gatsby-link"
import bg from '../images/hero4.jpg'
import E from '../images/echo-e.svg'
import E2 from '../images/e-outline.svg'
import { rhythm, scale } from "../utils/typography"
import Section from "../components/Section"
import Client from "../components/Client"
import Service from "../components/Service"
import WorkItem from "../components/WorkItem"
import Bg from "../components/Bg"
import Testimonial from "../components/Testimonial"
import PaginationStyles from "../components/PaginationStyles"
import Step from "../components/Step"
import Button from "../components/Button"
import Img from 'gatsby-image'
import styled from 'styled-components'
import Helmet from 'react-helmet'
import config from '../utils/config'
import Siema from 'siema'
import ArrowRight from '../images/arrow-right.svg'

const steps = [
  {
    id: 1,
    name: 'Plan',
    image: '',
    description: 'Discover business problems and discuss the most cost effective solutions.'
  },
  {
    id: 2,
    name: 'Build',
    image: '',
    description: 'Design, develop, and implement solutions in agile sprints to quickly deliver products.'
  },
  {
    id: 3,
    name: 'Optimize',
    image: '',
    description: 'Analyze and tweak to squeeze out the best performance and conversion.'
  }
]

const Pagination = styled.ul`${PaginationStyles}`
const HeaderText = styled.h1`
  font-size: ${scale(1).fontSize};
  line-height: 1;
  @media screen and (min-width: 992px) {
    font-size: ${scale(1.8).fontSize};
  }
`
const IntroImage = styled.div`
    display: block;
    margin: 0 auto;
    position: relative;
    top: 50%;
    max-width: 90%;
    // margin-left: -80px;
    transform: translateY(-50%);
    background-size: cover;
    background-position: center right;
    background-image: url('${props => props.bg}');
    background-color: ${config.primaryColor} !important;    
    svg {
      display: block;
      overflow: visible;
      path {
        stroke: ${config.primaryColor};
      }
    }
    .gatsby-image-outer-wrapper {
      overflow: hidden;
      position: absolute !important;
      top: 0;
      z-index: -1;
      height: 100%;
      width: 100%;
      .gatsby-image-wrapper {
        // transform: translate3d(-30px,0,0);
        overflow: visible !important;
        // transition: transform .5s ease-in-out !important;
      }
    }
  @media screen and (max-width: 992px) {
    .gatsby-image-outer-wrapper {
      opacity: .3;
    }
  }
`

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentSlide: 0,
      imageLoaded: false,
      imageStyles:  {
        transform: 'translate3d(-30px,0,0)',
        transition: 'transform .5s ease-in-out'
      }
    }
  }
  componentDidMount() {
    this.carousel = new Siema({
      selector: this.refs.carousel,
      onChange: () => this.setState({currentSlide: this.carousel.currentSlide})
    });
  }
  render() {
    const data = this.props.data
    let slider
    const categories = data.allWordpressWpServices.edges.filter(service => service.node.wordpress_parent === 0)
    const schemaOrg = {
      "@context": "http://schema.org",
      "@type": "Organization",
      "name": config.siteTitle,
      "url": config.siteUrl,
      "email": "hello@echo5digital.com",
      "logo": {
        "@context": "http://schema.org",
        "@type": "ImageObject",
        "url": config.staticLogo,
      },
      "sameAs" : [ 
        "https://www.facebook.com/echo5digital/",
        "https://www.twitter.com/echo5digital",
        "https://plus.google.com/116658731333038270091"
      ],
      "contactPoint": [
        { "@type": "ContactPoint",
          "telephone": "+1-678-667-1385",
          "contactType": "sales"
        }
      ]
    }

    return (
      <div>
        <Helmet
          title= 'Echo 5 - Atlanta Web Design &amp; Custom Software Development Company'
          titleTemplate=""
          meta={[
          { name: 'description', content: 'Echo 5 is a web design and custom software company based in Atlanta, Georgia.  We build high value ROI solutions to grow your business. Free consultation on request.' },
          ]}
        >
          <script type="application/ld+json">
            {JSON.stringify(schemaOrg)}
          </script>
        </Helmet>
        <Section hero>
          <Bg bgColor={config.primaryColor} right="0px" width="40%" overflow="visible">
          <IntroImage>
            <E2/>
            <Img sizes={this.props.data.hero.resolutions} />
          </IntroImage>
          </Bg>
          <div className="row">
            <div className="col-lg-7">
              <HeaderText>
                We are a digital agency in Atlanta<span className="primary">.</span>
              </HeaderText>
              {/* <p>Fully customized websites, design, and custom software that focus on driving results and growing your business.</p> */}
              <h3 className="font-weight-light">Web design, branding, and custom software to drive results and grow your business.</h3>
              <Link to="#how" className="mr-3">What we do <ArrowRight/></Link>
              <Link to="/work/">See case studies <ArrowRight/></Link>
            </div>
          </div>
        </Section>
        <Section nopad>
          <div className="row">
          {data.allWordpressWpClients.edges.map(({ node }) => (
            <div className="col-6 mx-auto col-sm-5ths" key={node.id}>
              <Client nopad node={node} />
            </div>
          ))}
          </div>
        </Section>

        <Section style={{paddingBottom: 0}} id="how">
          <div className="row">
            <div className="col-md-9 mx-auto">
              <blockquote className="text-center">Businesses bleed customers without a digital strategy and waste valuable time without the right software.  We provide high ROI solutions to grow your business.</blockquote>
            </div>
          </div>
        </Section>

        <Section>
          <div className="row">
            <div className="col-md-4">
              <h4 className="line">How we help</h4>
            </div>
            <div className="col-md-8">
              <div className="row">
                {categories.map(({ node }) => (
                  <div className="col-md-6" key={node.id}>
                    <Service node={node} children={data.allWordpressWpServices.edges.filter(service => service.node.wordpress_parent === node.wordpress_id)} />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </Section>

        <Section gray>
          <h4 className="line">Result-driven process</h4>
          <div className="row">
            {steps.map(( step ) => (
              <div className="col-md-4" key={step.id}>
                <Step node={step} />
              </div>
            ))}
          </div>
        </Section>
        
        <Section>
          <div className="row no-gutters">
            {data.allWordpressWpWork.edges.map(({ node }) => (
              <div className="col-md-6" key={node.id} >
                <WorkItem node={node} />
              </div>
            ))}          
          </div>
        </Section>

        <Section>
          <div className="row">
            <div className="col-lg-10 mx-auto">
              <div ref="carousel">
                {data.allWordpressWpTestimonials.edges.map(({ node }) => (
                  <Testimonial node={node} key={node.id} />
                ))}
              </div>   
              <Pagination>
                {data.allWordpressWpTestimonials.edges.map(( {node}, index ) => (
                  <li key={node.id}><a className={this.state.currentSlide === index ? 'active' : null} onClick={() => this.carousel.goTo(index)}>{ index + 1 }</a></li>
                ))}
              </Pagination>    
            </div>
          </div>
        </Section>

        <Section dark bgColor="transparent">
          <Bg bgColor='black' width="80%" />
          <div className="row">
            <div className="col-9">
              <h2>Ready to lead your industry?</h2>
              <h3>
                {/* <Link style={{ color: config.primaryColor, textDecoration: 'underline'}} to="/contact">Let's talk about it</Link> */}
                {/* <Link style={{ color: config.primaryColor, textDecoration: 'underline'}} to="/contact">Let's start the journey</Link> */}
                <Link style={{ color: config.primaryColor, textDecoration: 'underline'}} to="/contact">Click here to start the journey</Link>
              </h3>
            </div>
          </div>
        </Section>

      </div>
    )
  }
}

export default Home

// Set here the ID of the home page.
export const pageQuery = graphql`
  query homePageQuery {
    allWordpressWpClients(limit: 5) {
      edges {
        node {
          id
          title
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 160,) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
        }
      }
    }
    allWordpressWpServices {
      edges {
        node {
          id
          slug
          title
          excerpt
          wordpress_id
          wordpress_parent
          acf {
            color
            icon
          }
        }
      }
    }
    allWordpressWpWork(limit: 2) {
      edges {
        node {
          id
          slug
          title
          featured_media {
            localFile {
              childImageSharp {
                resolutions(width: 620, height: 428) {
                  ...GatsbyImageSharpResolutions_withWebp_tracedSVG
                }
              }
            }
          }
        }
      }
    }
    allWordpressWpTestimonials(limit: 4) {
      edges {
        node {
          id
          title
          content
          featured_media {
            localFile {
              childImageSharp {
                resolutions(width: 150, height: 150) {
                  ...GatsbyImageSharpResolutions_withWebp_tracedSVG
                }
              }
            }
          }
        }
      }
    }
    hero: imageSharp(id: { regex: "/hero4.jpg/" }) {
      resolutions(width: 600, height: 620, cropFocus: EAST) {
        ...GatsbyImageSharpResolutions
      }
    }
  }
`
