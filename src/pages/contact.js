import React, { Component } from "react"
import PropTypes from "prop-types"
import Section from "../components/Section"
import Bg from "../components/Bg"
import { rhythm } from "../utils/typography"
import bgContact from '../images/pinecone.jpeg'
import styled from 'styled-components'
import Helmet from 'react-helmet'
import config from '../utils/config'
import { Formik } from 'formik'
import Img from 'gatsby-image'


const ContactForm = styled.div`
  @media(min-width: 768px) {
    background: #fff;
    padding: 50px;
  }
`
const Error = styled.div`
  font-size: .7rem;
  color: #f7236e;
  position: absolute;
`

const Alert = styled.div`
  padding: 12px 24px;
  background: ${props => props.type === 'error' ? '#f7236e' : '#6bbf8d'};
  color: white;
  margin-bottom: ${rhythm(1)};
`


class Contact extends Component {

  render() {
    const data = this.props.data
    return (
      <div>
        <Helmet 
          title={`Contact`} 
          meta={[
          { name: 'description', content: 'Echo 5 Atlanta Web Design offers a one hour free consultation for your project or your current website.  Contact us to get a free quote now.' },
          ]}
        />                      
        <Section nopad className="mt-6">
          <Bg width="40%" className="d-none d-md-block">
            <Img resolutions={data.hero.resolutions} />
          </Bg>
          <div className="row">
            <div className="col-md-6">
            </div>
            <div className="col-md-6">
              <ContactForm>
                <Formik
                  initialValues={{
                    fullname: '',
                    email: '',
                    phone: '',
                    company: '',
                    message: '',
                  }}
                  validate={values => {
                    // same as above, but feel free to move this into a class method now.
                    let errors = {};
                    if (!values.fullname) {
                      errors.fullname = 'What should we call you?'
                    }
                    if (!values.message) {
                      errors.message = 'Give us a little heads up about what you want to talk about'
                    }
                    if (!values.email) {
                      errors.email = 'Don\'t worry, we won\'t spam you'
                    } else if (
                      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
                    ) {
                      errors.email = 'We need a real email address :)';
                    }
                    return errors;
                  }}
                  onSubmit={(
                    values,
                    { setSubmitting, setErrors, setStatus, resetForm }
                  ) => {
                    const formData = new FormData();
                    Object.keys(values).forEach(key => formData.append(key, values[key]));
                    fetch(config.apiUrl + '/contact-form-7/v1/contact-forms/767/feedback', {
                      method: 'post',
                      body: formData
                    })
                    .then((response) => {
                      return response.json()
                    })
                    .then((data) => {
                      setSubmitting(false)
                      if (data.status === 'mail_sent') {
                        dataLayer.push({'event' : 'form_submitted'})
                        resetForm()
                      }
                      setStatus({type: data.status === 'mail_sent' ? 'success' : 'error', message: data.message})
                    })
                    .catch(error => {
                      setSubmitting(false)
                    });
                  }}
                  render={({
                    values,
                    errors,
                    touched,
                    status,
                    setStatus,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                  }) => (
                  <form onSubmit={handleSubmit} ref="contactForm">
                    <h1>Let's chat</h1>   
                    {status && status.message && <Alert type={status.type}>{status.message}</Alert>}
                    <div className="row narrow-gutters">
                      <div className="form-group col-md-6">
                          <label>Name*</label>
                          <input 
                            type="text"
                            name="fullname" 
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.fullname} 
                          />
                          {touched.fullname && errors.fullname && <Error>{errors.fullname}</Error>}
                      </div>
                      <div className="form-group col-md-6">
                          <label>Email*</label>
                          <input
                            type="email" 
                            name="email"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.email} 
                          />
                          {errors.email &&
                          touched.email && <Error>{errors.email}</Error>}
                      </div>
                    </div>    
                    <div className="row narrow-gutters">
                      <div className="form-group col-md-6">
                          <label>Phone</label>
                          <input
                            type="phone"
                            name="phone"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.phone}
                          />
                      </div>
                      <div className="form-group col-md-6">
                          <label>Company</label>
                          <input
                            type="text"
                            name="company"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.company}
                          />
                      </div>
                    </div>       
                    <div className="form-group">
                        <label>Tell us a bit about your project</label>
                        <textarea
                          name="message"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.message}
                        ></textarea>
                        {errors.message &&
                        touched.message && <Error>{errors.message}</Error>}
                    </div>
                    <input className="btn btn-primary" type="submit" value="Request Consultation" disabled={isSubmitting} />
                  </form>
                  )}
                  />
              </ContactForm>

            </div>
          </div>
        </Section>

        <Section>
          <div className="row">
            <div className="col-md-4 mb-5">
              <h3>New projects</h3>
              <a href="mailto:hello@echo5web.com">hello@echo5digital.com</a><br/>
              <a href="tel:+1-678-667-1385">(678) 667-1385</a>
            </div>
            <div className="col-md-4 mb-5">
              <h3>Join us</h3>            
              <a href="mailto:careers@echo5web.com">careers@echo5digital.com</a><br/>
              <a href="tel:+1-678-667-1385">(678) 667-1385</a>
            </div>
            <div className="col-md-4 mb-5">
              <h3>Mailing Address</h3>
              <p>741 Monroe Drive, NE<br/>
              Atlanta, Georgia 30308</p>
            </div>
          </div>
        </Section>
        
      </div>
    )
  }
}

export default Contact

// Set here the ID of the home page.
export const pageQuery = graphql`
  query contactPageQuery {
    hero: imageSharp(id: { regex: "/pinecone.jpeg/" }) {
      resolutions(width: 680, height: 740) {
        ...GatsbyImageSharpResolutions
      }
    }
  }
`