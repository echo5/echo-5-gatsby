import React, { Component } from "react"
import PropTypes from "prop-types"
import Section from "../components/Section"
import Client from "../components/Client"
import Bg from "../components/Bg"
import Link from "gatsby-link"
import Helmet from 'react-helmet'
import config from '../utils/config'
import about2 from '../images/about2.jpg'
import Medal from '../images/medal.svg'
import styled from 'styled-components'
import { rhythm } from "../utils/typography"

const Awards = styled.div`
  width: 100%;
  svg {
    width: 30px;
    height: 30px;
    margin: 0 10px -10px 0 ;
  }
`

class About extends Component {
  render() {
    const data = this.props.data

    return (
      <div>
        <Section>
          <Helmet
            title={`About us`}
            meta={[
            { name: 'description', content: 'Echo 5 Atlanta Web Design is a web design and web development company based in Atlanta, Georgia.  We build professional websites and applications to help grow your business.' },
            ]}
          />                        
          <div className="row">
            <div className="col-md-6">
              <h1>You do you.<br/>We do digital.</h1>
              {/* <p>Just a team of guys and gals who love good code, inspirational design, and cold beer.</p> */}
              {/* <h2>We're all about letting companies get back to do what they do best.</h2> */}
            </div>
          </div>
        </Section>

        <Section half>
          <Bg fill cut img={about2} width="100%" h="50vh"/>
        </Section>

        <Section style={{paddingBottom: 0}}>
          <div className="row">
            <div className="col-md-8 mx-auto">
              <blockquote className="text-center">Becoming an extension of your team to help you grow to be the business you should be.  <strong>Every step of the way.</strong></blockquote>
            </div>
          </div>
        </Section>

        <Section>
          <div className="row">
            <div className="col-md-4">
              <h4 className="line">What we value</h4>
            </div>
            <div className="col-md-8">
              <h3>Measurable results</h3>
              <p>Beautiful websites are great, but a tool that drives your business is even better.  We focus on measurable metrics that further business profit.</p>
              <h3>Respecting budgets</h3>
              <p>No one likes surprises when it comes to billing.  We believe in being upfront about costs and stretching your dollar to return the best ROI.</p>
              <h3>No outsourcing</h3>
              <p>We value high quality work and found that we haven't been able to deliver the same results out of house.  We manage, design, and develop internally and will never hand you off to another company.</p>
              <h3>No cookie cutters</h3>
              <p>Your business is not like any other.  Your branding and marketing material should also deserves to be unique and stand out.</p>
              {/* <h3>Quick to market</h3>
              <p></p> */}
            </div>
          </div>
        </Section>

        <Section dark>
          <h2>Award-winning<span className="primary">.</span>  Seriously<span className="primary">.</span></h2>
          <div className="row col-md-8">
            <Awards width="100%">
                <div className="row">
                  <div className="col-md-6 mb-4"><Medal />Unmatched Style</div>
                  <div className="col-md-6 mb-4"><Medal />Responsive CSS</div>
                </div>
                <div className="row">
                  <div className="col-md-6 mb-4"><Medal />CSSREEL</div>
                  <div className="col-md-6 mb-4"><Medal />CSSAwards</div>
                </div>
                <div className="row">
                  <div className="col-md-6 mb-4"><Medal />Best CSS Award</div>
                  <div className="col-md-6 mb-4"><Medal />Design Nominees</div>
                </div>
                <div className="row">
                  <div className="col-md-6 mb-4"><Medal />Ray Templates</div>
                  <div className="col-md-6 mb-4"><Medal />CSSAWDS</div>
                </div>
                <div className="row">
                  <div className="col-md-6 mb-4"><Medal />Golden Web Awards</div>
                  <div className="col-md-6 mb-4"><Medal />BESTCSS</div>
                </div>
                <div className="row">
                  <div className="col-md-6 mb-4"><Medal />CSS Design House</div>
                  <div className="col-md-6 mb-4">&nbsp;</div>
                </div>
            </Awards>
          </div>
          </Section>

        <Section>
          <h2 className="text-center">A few companies we've helped grow</h2>
          <div className="row">
          {data.allWordpressWpClients.edges.map(({ node }) => (
            <div className="col-md-3 col-6" key={node.id}>
              <Client node={node} />
            </div>
          ))}
          </div>
        </Section>

        <Section dark bgColor="transparent">
          <Bg bgColor='#000' width="80%" />          
          <div className="row">
            <div className="col-8">
              <h2>Learn how we can help</h2>
              <h3><Link className="line" style={{ color: config.primaryColor, textDecoration: 'underline'}} to="/services">See services</Link></h3>
            </div>
          </div>
        </Section>
        
      </div>
    )
  }
}

export default About

export const pageQuery = graphql`
  query aboutPageQuery {
    allWordpressWpClients(limit: 15) {
      edges {
        node {
          id
          title
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 160,) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
        }
      }
    }
  }
`
