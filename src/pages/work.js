import React, { Component } from "react"
import PropTypes from "prop-types"
import Section from "../components/Section"
import WorkItem from "../components/WorkItem"
import Link from "gatsby-link"
import Helmet from 'react-helmet'
import config from '../utils/config'

import { rhythm } from "../utils/typography"

class Blog extends Component {
  render() {
    const data = this.props.data

    return (
      <div>
        <Helmet 
          title={`Work`} 
          meta={[
          { name: 'description', content: 'Echo 5 Web Design in Atlanta offers Laravel development, Ruby on Rails development, custom WordPress websites, responsive design, and e-commerce sites.' },
          ]}
        />              
        <Section className="pb-0">
          <div className="row">
            <div className="col-md-9">
              <h1>Featured work</h1>
              <p>Web design case studies, apps, and software development projects</p>
            </div>
          </div>
        </Section>
        <Section>
          <div className="row no-gutters">
          {data.allWordpressWpWork.edges.map(({ node }) => (
            <div className="col-md-6" key={node.id}>
              <WorkItem node={node} />
            </div>
          ))}
          </div>
        </Section>
      </div>
    )
  }
}

export default Blog

export const pageQuery = graphql`
  query workPageQuery {
    allWordpressWpWork(limit: 20) {
      edges {
        node {
          id
          slug
          title
          featured_media {
            localFile {
              childImageSharp {
                resolutions(width: 620, height: 428) {
                  ...GatsbyImageSharpResolutions_withWebp_tracedSVG
                }
              }
            }
          }
        }
      }
    }
  }
`
