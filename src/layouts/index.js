import React from "react"
import Helmet from "react-helmet"
import PropTypes from "prop-types"
import Modal from "../components/Modal"
import Section from "../components/Section"
import Button from "../components/Button"
import Link from "gatsby-link"
import styled from "styled-components"
import logoLight from'../images/logo-light.png'
import logoDark from '../images/logo-dark.png'
import Logo from '../images/logo.svg'
import Phone from '../images/phone.svg'
import favicon from '../images/favicon.png';
import config from '../utils/config'
import './app.scss'
import { rhythm, scale } from "../utils/typography"

const Navbar = styled.div`
  padding: 12px 30px;
  display: flex;
  width: 100%;
  position: absolute;
  top: 0;
  z-index: 100;
  align-items: center;
  font-size: .9rem;
`

const Cta = styled.div`
  margin-left: auto;
  position: fixed;
  top: 0px;
  right: 0px;
  z-index: 100;
  transition: .3s ${config.transition};
  a {
    background: black;
    height: 57px;
  }
  @media (max-width: 991px) {  
    right: 57px;
  }
  @media (max-width: 480px) {  
    opacity: 0;
    visibility: hidden;
    left: 0;
  }
  &.visible {
    opacity: 1;
    visibility: visible;
  }
`

const Nav = styled.ul`
  list-style: none;
  margin: 0px;
  li {
    float: left;
    margin-right: .9rem;
    margin-bottom: 0;
  }
  @media (max-width: 991px) {
    background: white;
    position: fixed;
    left: 100%;
    top: 0;
    height: 100%;
    width: 100%;
    max-width: 380px;
    display: flex;
    flex-direction: column;
    font-size: ${scale(0).fontSize};
    z-index: 90;
    transition: .3s ${config.transition};
    color: black;
    overflow: scroll;
    border-left: 6px solid black;
    li {
      &:first-child {
        margin-top: ${rhythm(3)};
      }
      margin-bottom: ${rhythm(1)};
    }
    &.active {
      transform: translate3d(-100%,0,0);
    }
  }
`;

const LogoWrapper = styled.div`
  display: inline-flex;
  a {
    display: inline-flex;
    align-items: center;
  }
  svg {
    width: 80px;
    margin: 0;
  }
`

const Navbutton = styled.div`
  background: white;
  border-left: 1px solid #cecece;
  border-bottom: 1px solid #cecece;
  z-index: 100;
  position: fixed;
  top: 0;
  right: 0;
  width: 57px;
  height: 57px;
  @media (min-width: 992px) {  
    display: none;
  }

  > div {

    background: black;
    height: 2px;
    width: 30px;
    overflow: visible;
    position: absolute;
    top: 50%;
    left: 13px;

    &:before {
      content: '';
      display: block;
      height: 2px;
      width: 30px;
      bottom: 10px;
      position: absolute;
      background: black;
      transition: all .2s ${config.transition};
    }

    &:after {
      content: '';
      display: block;
      height: 2px;
      width: 30px;
      top: 10px;
      position: absolute;
      background: black;
      transition: all .2s ${config.transition};
    }
  }

  &.active {
    > div {
      /* transform: rotate(225deg); */
      /* transition-delay: 0.12s; */
      /* transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); */

      &::before {
        /* top: 0; */
        width: 15px;
        top: -5px;
        right: -3px;
        transform: rotate(45deg);
      }

      &::after {
        width: 15px;
        top: 5px;
        right: -3px;
        transform: rotate(-45deg);
      }
    }
  }
`

class DefaultLayout extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isNavOpen: false,
      isScrolled: false
    }
  }
  componentDidMount() {
    var WebFont = require('webfontloader');
    WebFont.load({
      typekit: {
        id: 'wzw7hck'
      }
    })
    window.addEventListener('scroll', this.handleScroll.bind(this))

    // Load Google Optimize
    if (typeof dataLayer !== 'undefined') {
      dataLayer.push({'event': 'optimize.activate'})
    }
  }
  handleScroll (event) {
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop
    if (scrollTop > 400) {
      this.setState({
        isScrolled: true
      })
    } else {
      this.setState({
        isScrolled: false
      })
    }
  }
  openModal(event) {
    event.preventDefault()
    this.refs.modal.openModal()
  }
  toggleNav() {
    this.setState( prevState => ({
      isNavOpen: !prevState.isNavOpen
    }))
  }
  render() {
    return (
      <div>
        <Helmet>
          <link rel="shortcut icon" type="image/png" href={favicon} />
          <script async type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
        </Helmet>
        <Helmet 
          titleTemplate={`%s - ${config.siteTitle}`} 
          defaultTitle="Atlanta Web Design Company &amp; Digital Marketing Agency"
        />
        <Navbar className="navbar">
          <LogoWrapper>
            <Link
              to="/">
              <Logo />
            </Link>
          </LogoWrapper>
          <div>
          <Nav className={this.state.isNavOpen ? 'active' : 'hidden'}>
            <li><Link onClick={this.toggleNav.bind(this)} activeClassName="active" to="/about/">About</Link></li>
            <li><Link onClick={this.toggleNav.bind(this)} activeClassName="active" to="/services/">Services</Link></li>
            <li><Link onClick={this.toggleNav.bind(this)} activeClassName="active" to="/work/">Work</Link></li>
            <li><Link onClick={this.toggleNav.bind(this)} activeClassName="active" to="/blog/">Blog</Link></li>
            <li><Link onClick={this.toggleNav.bind(this)} activeClassName="active" to="/contact/">Contact</Link></li>
          </Nav>
          </div>
          <Cta className={this.state.isScrolled || this.state.isNavOpen ? 'visible' : ''}>
            <Button to="#" onClick={this.openModal.bind(this)} className="open-modal">
              <Phone /> Schedule a call
            </Button>
          </Cta>
          <Navbutton onClick={this.toggleNav.bind(this)}  className={this.state.isNavOpen ? 'active' : 'hidden'}>
            <div />
          </Navbutton>
        </Navbar>

        <Modal ref="modal">
          <div className="calendly-inline-widget" data-url="https://calendly.com/echo5" style={{minWidth: '100%', height:'500px'}}></div>
        </Modal>

        <div className="main">{this.props.children()}</div>

        <Section className={`pt-5 pb-5 ${this.props.location.pathname.startsWith('/blog') || this.props.location.pathname.startsWith('/contact') ? 'd-none' : '' }`}>
          <div className="row">
            <div className="col-sm-6">
              741 Monroe Drive, NE<br/>
              Atlanta, Georgia 30308<br/>
              <a href="tel:+1-678-667-1385">(678) 667-1385</a>   
            </div>
            <div className="col-sm-6 text-right">
              <ul className="list-unstyled mb-0">
                <li className="mb-0"><a href="/services/web-design/">Web design</a></li>
                <li className="mb-0"><a href="/services/web-development/">Web development</a></li>
                <li className="mb-0"><a href="/services/custom-software/">Custom software development</a></li>
                <li className="mb-0"><a href="/services/digital-marketing/">Digital marketing</a></li>
              </ul>
            </div>
       
          </div>
        </Section>

      </div>
    )
  }
}

DefaultLayout.propTypes = {
  location: PropTypes.object.isRequired,
}

export default DefaultLayout
