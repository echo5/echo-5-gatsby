exports.onRouteUpdate = (state, page, pages) => {
  // Load Google Optimize
  if (typeof dataLayer !== 'undefined') {
    dataLayer.push({'event': 'optimize.activate'})
  }
};