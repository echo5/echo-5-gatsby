const _ = require(`lodash`)
const Promise = require(`bluebird`)
const path = require(`path`)
const slash = require(`slash`)
const createPaginatedPages = require(`gatsby-paginate`)

// Implement the Gatsby API “createPages”. This is
// called after the Gatsby bootstrap is finished so you have
// access to any information necessary to programmatically
// create pages.
// Will create pages for Wordpress pages (route : /{slug})
// Will create pages for Wordpress posts (route : /post/{slug})
exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators
  return new Promise((resolve, reject) => {
    // The “graphql” function allows us to run arbitrary
    // queries against the local Wordpress graphql schema. Think of
    // it like the site has a built-in database constructed
    // from the fetched data that you can run queries against.

    // ==== PAGES (WORDPRESS NATIVE) ====
    // graphql(
    //   `
    //     {
    //       allWordpressPage {
    //         edges {
    //           node {
    //             id
    //             slug
    //             status
    //             template
    //           }
    //         }
    //       }
    //     }
    //   `
    // )
      // .then(result => {
      //   if (result.errors) {
      //     console.log(result.errors)
      //     reject(result.errors)
      //   }

      //   // Create Page pages.
      //   const pageTemplate = path.resolve(`./src/templates/page.js`)
      //   // We want to create a detailed page for each
      //   // page node. We'll just use the Wordpress Slug for the slug.
      //   // The Page ID is prefixed with 'PAGE_'
      //   // _.each(result.data.allWordpressPage.edges, edge => {
      //   //   // Gatsby uses Redux to manage its internal state.
      //   //   // Plugins and sites can use functions like "createPage"
      //   //   // to interact with Gatsby.
      //   //   createPage({
      //   //     // Each page is required to have a `path` as well
      //   //     // as a template component. The `context` is
      //   //     // optional but is often necessary so the template
      //   //     // can query data specific to each page.
      //   //     path: `/${edge.node.slug + '/'}/`,
      //   //     component: slash(pageTemplate),
      //   //     context: {
      //   //       id: edge.node.id,
      //   //     },
      //   //   })
      //   // })
      // })
      // ==== END PAGES ====

      // ==== POSTS (WORDPRESS NATIVE AND ACF) ====
      // .then(() => {
        graphql(
          `
            {
              allWordpressPost(sort: { order: DESC, fields: [date] }) {
                edges {
                  node {
                    id
                    wordpress_id
                    title
                    excerpt
                    slug
                    date(formatString: "MMMM DD, YYYY")
                    author {
                      name
                    }
                    featured_media {
                      localFile {
                        childImageSharp {
                          sizes(maxWidth: 500, maxHeight: 280) {
                            base64
                            aspectRatio
                            src
                            srcSet
                            sizes
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          `
        ).then(result => {
          if (result.errors) {
            console.log(result.errors)
            reject(result.errors)
          }
          createPaginatedPages({
            edges: result.data.allWordpressPost.edges,
            createPage: createPage,
            pageTemplate: "src/templates/blog.js",
            pageLength: 10, // This is optional and defaults to 10 if not used
            pathPrefix: "blog", // This is optional and defaults to an empty string if not used
          })
          const postTemplate = path.resolve(`./src/templates/post.js`)
          // We want to create a detailed page for each
          // post node. We'll just use the Wordpress Slug for the slug.
          // The Post ID is prefixed with 'POST_'
          _.each(result.data.allWordpressPost.edges, edge => {
            createPage({
              path: 'blog/' + edge.node.slug + '/',
              component: slash(postTemplate),
              context: {
                id: edge.node.id,
                postId: edge.node.wordpress_id
              },
            })
          })
          resolve()
        })
      // })
    // ==== END POSTS ====
    // ==== WORK ====
      .then(() => {
        graphql(
          `
            {
              allWordpressWpWork {
                edges {
                  node {
                    id
                    slug
                    status
                  }
                }
              }
            }
          `
        ).then(result => {
          if (result.errors) {
            console.log(result.errors)
            reject(result.errors)
          }
          const workItemTemplate = path.resolve(`./src/templates/work_item.js`)
          // We want to create a detailed page for each
          // post node. We'll just use the Wordpress Slug for the slug.
          // The Post ID is prefixed with 'POST_'
          _.each(result.data.allWordpressWpWork.edges, edge => {
            createPage({
              path: 'work/' + edge.node.slug + '/',
              component: slash(workItemTemplate),
              context: {
                id: edge.node.id,
              },
            })
          })
          resolve()
        })
      })
    // ==== END WORK ====
    // ==== SERVICES ====
      .then(() => {
        graphql(
          `
            {
              allWordpressWpServices {
                edges {
                  node {
                    id
                    slug
                    status
                    template
                    excerpt
                  }
                }
              }
            }
          `
        ).then(result => {
          if (result.errors) {
            console.log(result.errors)
            reject(result.errors)
          }
          const serviceTemplate = path.resolve(`./src/templates/service.js`)
          // We want to create a detailed page for each
          // post node. We'll just use the Wordpress Slug for the slug.
          // The Post ID is prefixed with 'POST_'
          _.each(result.data.allWordpressWpServices.edges, edge => {
            if (edge.node.excerpt) {
              createPage({
                path: 'services/' + edge.node.slug + '/',
                component: slash(serviceTemplate),
                context: {
                  id: edge.node.id,
                },
              })
            }
          })
          resolve()
        })
      })
    // ==== END POSTS ====
  })
}
